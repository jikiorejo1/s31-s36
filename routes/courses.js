//[SECTION] Dependencies and Modules
    const exp = require("express");
    const controller = require('../controllers/courses');

//[SECTION] Routing Component
    const route = exp.Router()

//[SECTION] POST Routes
    route.post('/create', (req, res) => {
        let data = req.body;
        controller.createCourse(data).then(outcome =>{
            res.send(outcome);
        })
            
    })

//[SECTION] GET Routes

//[SECTION] PUT Routes

//[SECTION] DELETE Routes

//[SECTION] Export Route Systems
module.exports = route;