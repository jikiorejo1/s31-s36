//[SECTION] Dependencies and Modules
    const Course = require('../models/Course');

//[SECTION] Functionality [CREATE]
    module.exports.createCourse = (info) => {
        let cName = info.name;
        let cDesc = info.description;
        let cCost = info.price;
        let newCourse = new Course({
            name : cName,
            description : cDesc,
            price : cCost
        })
        return newCourse.save().then((savedCourse, error) => {
            if (error) {
                return 'Failed to save new Document.';
            } else {
                return savedCourse;
            }
        })
    }

//[SECTION] Functionality [RETRIEVE]

//[SECTION] Functionality [UPDATE]

//[SECTION] Functionality [DELETE]