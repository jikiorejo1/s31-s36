const express = require('express');
const mongoose = require('mongoose'); 
const dotenv = require('dotenv');
const courseRoutes = require('./routes/courses');

const app = express(); 
app.use(express.json()); 
app.use(express.urlencoded({extended:true}))
dotenv.config();
const port = process.env.PORT;
app.use(courseRoutes);

const secret = process.env.CONNECTION_STRING;
mongoose.connect(secret);
let dbStatus = mongoose.connection; 
dbStatus.once('open', () => console.log("We're Now Connected to the Database")); 

app.get('/', (req, res) => {
	res.send('Narito ka sa aking pahina! Berigud!');
});

app.listen(port, () => console.log(`Server is running on port: ${port}`));