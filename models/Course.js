const mongoose = require('mongoose');

    const courseSchema = new mongoose.Schema({
    name:{
        type: String,
        required: [true, 'Course Name is required.']    
    },
    description:{
        type: String,
        required: [true, 'Course Description is required.']
    },
    price:{
        type: Number,
        required: [true, 'Course Price is required.']    
    },
    isActive:{
        type: Boolean,
        default: true    
    },
    createdOn:{
        type: Date,
        default: new Date()
    },
    enrollees:[
        {
            userID: {
                type: String,
                required: [true, "User's ID is required"]
            },
            enrolledOn: {
                type: Date,
                default: Date()
            }
        }
    ]
})
    const Course = mongoose.model("Course", courseSchema);

    module.exports = Course;
